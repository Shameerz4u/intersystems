Class Email.SendEmailRequest Extends Ens.Request
{
	Property EmailAddress AS %String (MAXLEN = 1000);
	Property Subject AS %String (MAXLEN = 1000);
	Property Body AS %String (MAXLEN = 1000);
	
Storage Default
{
<Data name="SendEmailRequestDefaultData">
<Subscript>"SendEmailRequest"</Subscript>
<Value name="1">
<Value>EmailAddress</Value>
</Value>
<Value name="2">
<Value>Subject</Value>
</Value>
<Value name="3">
<Value>Body</Value>
</Value>
</Data>
<DefaultData>SendEmailRequestDefaultData</DefaultData>
<Type>%Library.CacheStorage</Type>
}

}