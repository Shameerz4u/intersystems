Class Email.SendEmailOperation extends Ens.BusinessOperation {

	Parameter ADAPTER = "EnsLib.EMail.OutboundAdapter";
	
	Property Adapter As EnsLib.EMail.OutboundAdapter;
	
	Parameter INVOCATION = "Queue";
	
	Method SendEmail(pInput As Email.SendEmailRequest, pOutput As Ens.Response) As %Status
	{
		set tSC = $$$OK
		try
		{
			/*
			set s=##class(%Net.SMTP).%New()
			set auth=##class(%Net.Authenticator).%New() ; use default authentication list
			set auth.UserName="shameerz4u@gmail.com"
			set auth.Password="Sulaiman1986"
			
			set s.authenticator=auth
			set s.smtpserver="smtp.gmail.com"
			set s.port=465
			set s.SSLConfiguration="Test"
			
			set m=##class(%Net.MailMessage).%New()
			set m.From="shameerz4u@gmail.com"
			do m.To.Insert("shameer@krinnovative.com")
			set m.Subject="Test mail"
			do m.TextData.Write("Testing Mail's")
			set status= ..Adapter.SendMail(m)
			if $$$ISERR(status) do $system.OBJ.DisplayError(status)
			quit*/
			
			set ..Adapter.SSLConfig = "Test"
			set email=##class(%Net.MailMessage).%New()
			
			#Dim email as %Net.MailMessage
			set email.From = "Shameerz4u@gmail.com"
			do email.To.Insert("shameer@krinnovative.com")
			set email.Subject = "Test"
			do email.TextData.Write(pInput.Body) 
			set email.ContentType = "text/html"
			set email.IsHTML=1
			set email.Charset = "utf-8"
			
			set tSC = ..Adapter.SendMail(email)
		}
		catch ex
		{
			set tSC = ex.AsStatus()
		}
		
		return tSC
	}

	XData MessageMap
	{
	<MapItems>
	  <MapItem MessageType="SendEmailRequest">
	    <Method>SendEmail</Method>
	  </MapItem>
	</MapItems>
}
}