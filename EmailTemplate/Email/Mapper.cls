Class Email.Mapper Extends %SerialObject
{
	Property FieldName AS %String [Required];
	Property PlaceHolderValue AS %String [Required];
Storage Default
{
<Data name="MapperState">
<Value name="1">
<Value>FieldName</Value>
</Value>
<Value name="2">
<Value>PlaceHolderValue</Value>
</Value>
</Data>
<State>MapperState</State>
<StreamLocation>^Email.MapperS</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}