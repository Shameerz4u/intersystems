Class Email.Template Extends %Persistent
{
	Index TemplateNameIdx On TemplateName;
	Property TemplateName AS %String [Required];
	Property EmailBodyContent AS %String [Required];
	Property EmailSubjectLine AS %String [Required];
	Property TemplateMapper AS array of Email.Mapper [Required];
	
	///Get the Template object based on the template name
	ClassMethod GetTemplateObjectByTemplateName(templateName AS %String, Output obj AS Email.Template) AS %Status
	{
		Set tSC = $$$OK
		
		try
		{	
			if (templateName '= ""){
				&sql(select ID into :id from Email.Template where TemplateName = :templateName)
				
				if (id > 0){
					set obj = ##class(Email.Template).%OpenId(id)
				}
				else{
					set tSC = $system.Status.Error(5760,"Template does not exist")
				}
			}
			else{
				set tSC = $system.Status.Error(5760,"Invalid Template Name")
			}
		}
		catch ex
		{
			write !, ex.Name
		}
		return tSC
	}
	
	///Get the Subject Line from the Email Template and construct it by replacing the place holder values 
	Method CreateSubjectLine(reqObj AS %DynamicObject,output subject AS %String) AS %Status
	{
		Set tSC = $$$OK
		
		try
		{	
			Set subject = ..EmailSubjectLine
			Set mapperObj = ..TemplateMapper
			Set mapperCount = mapperObj.Count()
			
			//Loop through each mapper object and find out the place holder value
			for i=1:1:mapperCount {
				Set key = ""
				Do mapperObj.GetNext(.key)
				
				if (key '= "") {
					Set placeHolder = mapperObj.GetAt(key).PlaceHolderValue
					Set placeHolderValue = reqObj.%Get(key)
					
					//Replace placeholder with the value from the dynamic object template params 
					Set subject = $Replace(subject,placeHolder,placeHolderValue)
				}
			}
			return tSC
		}
		catch ex
		{
			write !, ex.Name
		}
		return tSC
	}
	
	///Get the body content from the Email Template 
	Method CreateBodyContent(reqObj AS %DynamicObject,output body AS %String) AS %Status
	{
		Set tSC = $$$OK
		
		try
		{	
			Set body = ..EmailBodyContent
			Set mapperObj = ..TemplateMapper
			Set mapperCount = mapperObj.Count()
			
						
			//Loop through each mapper object and find out the place holder value
			for i=1:1:mapperCount {
			
				Do mapperObj.GetNext(.key)	
				
				
				if (key '= "") {
					Set placeHolder = mapperObj.GetAt(key).PlaceHolderValue
					Set placeHolderValue = reqObj.%Get(key)
					
										
					//Replace placeholder with the value from the dynamic object template params 
					Set body = $Replace(body,placeHolder,placeHolderValue)
					
				}
			}
			return tSC
		}
		catch ex
		{
			write !, ex.Name
		}
		return tSC
	}
Storage Default
{
<Data name="TemplateDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
<Value name="2">
<Value>TemplateName</Value>
</Value>
<Value name="3">
<Value>EmailBodyContent</Value>
</Value>
<Value name="4">
<Value>EmailSubjectLine</Value>
</Value>
</Data>
<Data name="TemplateMapper">
<Attribute>TemplateMapper</Attribute>
<Structure>subnode</Structure>
<Subscript>"TemplateMapper"</Subscript>
</Data>
<DataLocation>^Email.TemplateD</DataLocation>
<DefaultData>TemplateDefaultData</DefaultData>
<IdLocation>^Email.TemplateD</IdLocation>
<IndexLocation>^Email.TemplateI</IndexLocation>
<StreamLocation>^Email.TemplateS</StreamLocation>
<Type>%Library.CacheStorage</Type>
}

}