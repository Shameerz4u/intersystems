Class BusinessOperation.SendEmailOperation Extends Ens.BusinessOperation
{
	Parameter ADAPTER = "EnsLib.EMail.OutboundAdapter";
	
	Property Adapter As EnsLib.EMail.OutboundAdapter;
	
	Parameter INVOCATION = "Queue";
	
	Method SendEmail(pInput As Messages.SendEmailRequest, pOutput As Ens.Response) As %Status
	{
		set tSC = $$$OK
		try
		{
			set email=##class(%Net.MailMessage).%New()
			
			#Dim email as %Net.MailMessage

			do email.To.Insert(pInput.To)
			set email.Subject = pInput.Subject
			set email.Cc = pInput.CC
			set email.Bcc = pInput.BCC
			do email.TextData.Write(pInput.Body) 
			set email.ContentType = "text/html"
			set email.IsHTML=1
			set email.Charset = "utf-8"
			
			set tSC = ..Adapter.SendMail(email)
		}
		catch ex
		{
			set tSC = ex.AsStatus()
		}
		
		return tSC
	}

	XData MessageMap
	{
		<MapItems>
		  <MapItem MessageType="Messages.SendEmailRequest">
		    <Method>SendEmail</Method>
		  </MapItem>
		</MapItems>
	}
}