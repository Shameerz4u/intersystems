Class REST.EmailRestServer extends %CSP.REST
{
	XData UrlMap
	{
		<Routes>
			<Route Url="/SendEmail" Method="POST" Call="SendEmail"/>
		</Routes>
	}
	
	ClassMethod SendEmail() As %Status
	{
		set tSC = $$$OK
	
		If '..GetJSONFromRequest(.obj) {
		Set %response.Status = ..#HTTP400BADREQUEST
		Set error = {"errormessage": "JSON not found"}
		Write error.%ToJSON()
		Quit $$$OK
		}
		
		If '..ValidateJSON(obj,.error) {
			Set %response.Status = ..#HTTP400BADREQUEST
			Write error.%ToJSON()
			Quit $$$OK
		}
		
		Set template = ##class(Messages.EmailTemplateRequest).%New()
		Do ..CopyToEmailTemplateFromJSON(.template,obj)
	
		//Instantiate business service
		set tSC = ##class(Ens.Director).CreateBusinessService("Email Rest Service",.tService)
		
		$$$ThrowOnError(tSC)
		
		set tSC = tService.ProcessInput(template, .output)
						
		$$$ThrowOnError(tSC)
	
		return tSC
	}
	
	// HELPER METHODS
	
	/// Helper method
	ClassMethod ValidateJSON(obj As %DynamicObject, Output error As %DynamicObject) As %Boolean
	{
		Set error = {}
		
		If obj.%Get("TemplateName") = "" {
			Set error.errormessage = "Template Name was blank"
			Quit 0
		}	
		
		If obj.%Get("To") = "" {
			Set error.errormessage = "To Address was blank"
			Quit 0
		}
		
		Quit 1
	}
	
	/// Helper method
	ClassMethod CopyToEmailTemplateFromJSON(ByRef template As Messages.EmailTemplateRequest, obj As %DynamicObject) [ Private ]
	{
		set local = obj.%ToJSON()
		Set template.TemplateObj = local
		Set ^ss("Test1") = template.TemplateObj.To _ "IsObject" _ $IsObject(template.TemplateObj)
	}
	
	/// Helper method
	ClassMethod GetJSONFromRequest(Output obj As %DynamicObject) As %Boolean
	{
		Set ok = 1
		Try {
			Set obj = ##class(%DynamicObject).%FromJSON(%request.Content)
			//s result = ##class(Seasons.Library.Common.PersistentBaseAbstract).GetObjectFromJSONString(%request.Content,.obj)	
			Set ^ss("Test") = obj.To _ "IsObject" _ $IsObject(obj)
		} Catch ex {
			Set ok = 0
		}
		Quit ok
	}
}