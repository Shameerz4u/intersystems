Class Messages.EmailTemplateRequest Extends Ens.Request
{
	Property TemplateObj As %String(MAXLEN="");
	Property Test AS %String;
Storage Default
{
<Data name="EmailTemplateRequestDefaultData">
<Subscript>"EmailTemplateRequest"</Subscript>
<Value name="1">
<Value>TemplateName</Value>
</Value>
<Value name="2">
<Value>To</Value>
</Value>
<Value name="3">
<Value>CC</Value>
</Value>
<Value name="4">
<Value>BCC</Value>
</Value>
<Value name="5">
<Value>TemplateParams</Value>
</Value>
<Value name="6">
<Value>TemplateObj</Value>
</Value>
<Value name="7">
<Value>test</Value>
</Value>
<Value name="8">
<Value>Obj</Value>
</Value>
<Value name="9">
<Value>Test</Value>
</Value>
</Data>
<DefaultData>EmailTemplateRequestDefaultData</DefaultData>
<Type>%Library.CacheStorage</Type>
}

}