Class Messages.SendEmailRequest Extends Ens.Request
{
	Property To  AS %String;
	Property CC  AS %String;
	Property BCC  AS %String;
	Property Subject AS %String (MAXLEN = 1000);
	Property Body AS %String (MAXLEN = 1000);
Storage Default
{
<Data name="SendEmailRequestDefaultData">
<Subscript>"SendEmailRequest"</Subscript>
<Value name="1">
<Value>From</Value>
</Value>
<Value name="2">
<Value>To</Value>
</Value>
<Value name="3">
<Value>Subject</Value>
</Value>
<Value name="4">
<Value>Body</Value>
</Value>
<Value name="5">
<Value>CC</Value>
</Value>
<Value name="6">
<Value>BCC</Value>
</Value>
</Data>
<DefaultData>SendEmailRequestDefaultData</DefaultData>
<Type>%Library.CacheStorage</Type>
}

}