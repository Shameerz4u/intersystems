Class Messages.EmailTemplateResponse Extends Ens.Response
{
	Property Body AS %String;
	Property Subject AS %String; 
Storage Default
{
<Data name="EmailTemplateResponseDefaultData">
<Subscript>"EmailTemplateResponse"</Subscript>
<Value name="1">
<Value>Body</Value>
</Value>
<Value name="2">
<Value>Subject</Value>
</Value>
</Data>
<DefaultData>EmailTemplateResponseDefaultData</DefaultData>
<Type>%Library.CacheStorage</Type>
}

}