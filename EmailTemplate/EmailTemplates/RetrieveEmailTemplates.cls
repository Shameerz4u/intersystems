Class EmailTemplates.RetrieveEmailTemplates Extends %Persistent
{
	Property TemplateValue AS %String (MAXLEN = 10000);
	
	Method GetEmailTemplate(pInput As Messages.EmailTemplateRequest, pOutput As Messages.EmailTemplateResponse)
	{
		#Dim pInput As Messages.EmailTemplateRequest
		
		if (pInput.TemplateName = "ShipperBooking") {
			Set template = ##class(EmailTemplates.ShipperBookingEmailTemplate).%New()
			
			#Dim template As EmailTemplates.ShipperBookingEmailTemplate
			
			Set body = template.GetShipperBookingTemplateBody()
			Set param1 = pInput.TemplateParams.shipmentID
			Set param2 = pInput.TemplateParams.carrier
			Set body = Replace(body,"&1",param1)
			Set body = Replace(body,"&2",param2)
		}
	}
	
Storage Default
{
<Data name="RetrieveEmailTemplatesDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
<Value name="2">
<Value>TemplateValue</Value>
</Value>
</Data>
<DataLocation>^EmailTempl4FA2.RetrieveEma3204D</DataLocation>
<DefaultData>RetrieveEmailTemplatesDefaultData</DefaultData>
<IdLocation>^EmailTempl4FA2.RetrieveEma3204D</IdLocation>
<IndexLocation>^EmailTempl4FA2.RetrieveEma3204I</IndexLocation>
<StreamLocation>^EmailTempl4FA2.RetrieveEma3204S</StreamLocation>
<Type>%Library.CacheStorage</Type>
}

}