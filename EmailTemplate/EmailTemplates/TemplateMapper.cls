Class EmailTemplates.TemplateMapper Extends %SerialObject
{
	Property FieldName AS %String [Required];
	Property FieldValue AS %String [Required];

Storage Default
{
<Data name="TemplateMapperDefaultData">
<Subscript>"1"</Subscript>
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
</Data>
<Data name="TemplateMapperState">
<Value name="1">
<Value>FieldName</Value>
</Value>
<Value name="2">
<Value>FieldValue</Value>
</Value>
<Value name="3">
<Value>ElementType</Value>
</Value>
<Value name="4">
<Value>Size</Value>
</Value>
</Data>
<DataLocation>^EmailTemplates.TemplateMapperD</DataLocation>
<DefaultData>TemplateMapperDefaultData</DefaultData>
<IdLocation>^EmailTemplates.TemplateMapperD</IdLocation>
<IndexLocation>^EmailTemplates.TemplateMapperI</IndexLocation>
<State>TemplateMapperState</State>
<StreamLocation>^EmailTemplates.TemplateMapperS</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}