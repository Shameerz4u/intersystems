Class EmailTemplates.CarrierNotificationTemplate Extends (EmailTemplates.Template,%Persistent)
{
	Method GetCarrierNotificationTemplateBody() As %String
	{
		Set ..Body = "You have been matched for shipment &1"
		
		return ..Body
	}
	
	Method GetCarrierNotificationTemplateSubject() As %String
	{
		Set ..Subject = "You're matched for a NEW Shipment!"
		
		return ..Subject
	}
Storage Default
{
<Type>%Library.CacheStorage</Type>
}

}