Class EmailTemplates.EmailTemplate Extends %Persistent
{
	Index TemplateNameIdx On TemplateName;
	Property TemplateName AS %String [Required];
	Property EmailBodyContent AS %String [Required];
	Property EmailSubjectLine AS %String [Required];
	Property TemplateMapper AS array of EmailTemplates.TemplateMapper;
	
	///Get the Subject Line from the Email Template 
	Method GetSubjectLine(TemplateParams AS %DynamicObject) AS %String
	{
		return ..EmailSubjectLine
	}
	
	///Get the body content from the Email Template 
	Method GetBodyContent(TemplateParams AS %DynamicObject) AS %String
	{
		return ..EmailBodyContent
	}
Storage Default
{
<Data name="EmailTemplateDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
<Value name="2">
<Value>TemplateName</Value>
</Value>
<Value name="3">
<Value>EmailBodyContent</Value>
</Value>
<Value name="4">
<Value>EmailSubjectLine</Value>
</Value>
<Value name="5">
<Value>Mapper</Value>
</Value>
</Data>
<Data name="TemplateMapper">
<Attribute>TemplateMapper</Attribute>
<Structure>subnode</Structure>
<Subscript>"TemplateMapper"</Subscript>
</Data>
<DataLocation>^EmailTemplates.EmailTemplateD</DataLocation>
<DefaultData>EmailTemplateDefaultData</DefaultData>
<IdLocation>^EmailTemplates.EmailTemplateD</IdLocation>
<IndexLocation>^EmailTemplates.EmailTemplateI</IndexLocation>
<StreamLocation>^EmailTemplates.EmailTemplateS</StreamLocation>
<Type>%Library.CacheStorage</Type>
}

}