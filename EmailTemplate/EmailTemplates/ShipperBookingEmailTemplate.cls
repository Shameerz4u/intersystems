Class EmailTemplates.ShipperBookingEmailTemplate extends (EmailTemplates.Template,%Persistent)
{
	Method GetShipperBookingTemplateBody() As %String
	{
		Set ..Body = "Shipment &1 has been booked with carrier &2"
		
		return ..Body
	}
	
	Method GetShipperBookingTemplateSubject() As %String
	{
		Set ..Subject = "Your Shipment &1 has booked!!"
		
		return ..Subject
	}
	
Storage Default
{
<Type>%Library.CacheStorage</Type>
}

}